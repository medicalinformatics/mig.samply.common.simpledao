/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.sdao;

import de.samply.sdao.json.JsonResource;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

/**
 * This executor upgrades the database version from one version to the next. It can execute various
 * SQL files and then call a method in the implementing class.
 */
public abstract class UpgradeExecutor extends SqlConnection {

  /** The list of files that will be executed in the Upgrader. */
  private List<String> files;

  /**
   * Initializes this UpgradeExecutor with the given ConnectionProvider. The given files will be
   * executed as SQL statements.
   *
   * @param provider the connection provider
   * @param files a list of SQL files. Optional.
   */
  protected UpgradeExecutor(ConnectionProvider provider, String... files) {
    super(provider);

    this.files = Arrays.asList(files);
  }

  /** Executes the files that are necessary for the upgrade. */
  public void executeFiles() throws DaoException {
    for (String file : files) {
      Reader reader =
          new InputStreamReader(this.getClass().getResourceAsStream(file), StandardCharsets.UTF_8);
      executeStream(reader);
    }
  }

  /**
   * This method is called after the SQL files have been executed. Returns true, if the upgrade was
   * successful, false otherwise.
   */
  public abstract boolean executeUpgrade(boolean dryRun) throws DaoException;

  /** Returns the default configuration for the next database version. */
  public abstract JsonResource getDefaultConfig();
}
