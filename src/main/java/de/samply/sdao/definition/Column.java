/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.sdao.definition;

import java.io.Serializable;

public class Column implements Serializable {

  private static final long serialVersionUID = 2263961011322967732L;

  /** The table for this column. */
  public final Table table;

  /** The column name. */
  public final String column;

  /** The alias, usually "table_column". */
  public final String alias;

  /** If true, this column will be selected with the "DISTINCT" modifier. */
  public final boolean distinct;

  /** If false, this column will not be used when inserting new values. */
  public Column(Table table, String column) {
    this(table, column, false);
  }

  /** TODO: add javadoc. */
  public Column(Table table, String column, boolean distinct) {
    this.table = table;
    this.column = column;
    this.alias = table.alias + "_" + column;
    this.distinct = distinct;
  }

  public String access() {
    return "\"" + table.alias + "\".\"" + column + "\"";
  }

  public String column() {
    return "\"" + column + "\"";
  }

  public ColumnValue val(Object value) {
    return new ColumnValue(this, value);
  }

  public static class ColumnValue {
    public final Column column;
    public final Object value;

    public ColumnValue(Column column, Object value) {
      this.column = column;
      this.value = value;
    }
  }
}
