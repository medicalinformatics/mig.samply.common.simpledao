/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.sdao;

import de.samply.sdao.json.JsonResource;
import de.samply.sdao.json.Value;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * A basic connection provider that provides a SQL connection, implements AutoCloseable (should be
 * used in try blocks) and a way to create new DAOs.
 */
public abstract class ConnectionProvider extends SqlConnection implements AutoCloseable {

  protected final String host;
  protected final String database;
  protected final String username;
  protected final String password;

  /** The current userId, if necessary or applicable. */
  protected int userId;

  /** The map that stores all DAOs. */
  protected HashMap<Class<?>, Object> map = new HashMap<>();

  /** The SQL connection. */
  protected Connection connection;

  /** Initializes this connection provider with the given database credentials. */
  protected ConnectionProvider(
      String host, String database, String username, String password, int userId) {
    super(null);
    this.host = host;
    this.userId = userId;
    this.database = database;
    this.username = username;
    this.password = password;
  }

  /**
   * Closes the underlaying SQL connection. Executes a rollback. So before you close the connection,
   * make sure you commit your data.
   */
  @Override
  public final void close() throws DaoException {
    map.clear();
    try {
      if (connection != null) {
        connection.rollback();
        connection.close();
      }
    } catch (SQLException e) {
      throw new DaoException("Exception in Close!", e);
    }
  }

  /** Executes a rollback on the SQL connection if there is one. */
  public final void rollback() throws DaoException {
    try {
      if (connection != null) {
        connection.rollback();
      }
    } catch (SQLException e) {
      throw new DaoException("Exception in Rollback!", e);
    }
  }

  /**
   * Returns the SQL connection. Establishes one, if there is currently none. Disables the auto
   * commit feature.
   */
  @Override
  public final Connection get() throws SQLException {
    if (connection == null) {
      String url =
          "jdbc:postgresql://"
              + host
              + "/"
              + database
              + "?user="
              + username
              + "&password="
              + password;

      connection = DriverManager.getConnection(url);
      connection.setAutoCommit(false);
    }
    return connection;
  }

  /**
   * Returns an instance of the requested class. The requested class should be an instance of
   * AbstractDao.
   */
  @SuppressWarnings("unchecked")
  public final <T extends AbstractDao<?>> T get(Class<T> clazz) {
    try {
      if (!map.containsKey(clazz)) {
        Constructor<T> constructor = clazz.getDeclaredConstructor(ConnectionProvider.class);
        constructor.setAccessible(true);
        map.put(clazz, constructor.newInstance(this));
      }
      return (T) map.get(clazz);
    } catch (NoSuchMethodException
        | SecurityException
        | InstantiationException
        | IllegalAccessException
        | IllegalArgumentException
        | InvocationTargetException e) {
      e.printStackTrace();
      throw new UnsupportedOperationException();
    }
  }

  /** Executes a commit on the SQL connection if there is one. */
  public final void commit() throws DaoException {
    try {
      if (connection != null) {
        connection.commit();
      }
    } catch (SQLException e) {
      throw new DaoException("Exception in Commit!", e);
    }
  }

  /** Returns the current configuration. Uses the default configuration as template. */
  public final JsonResource getConfig() throws DaoException {
    ConfigDao dao = get(ConfigDao.class);

    JsonResource def = dao.get("defaultConfig");
    JsonResource current = dao.get("config");

    int dbVersion = def.getProperty("dbVersion").asInteger();

    for (String property : current.getDefinedProperties()) {
      def.removeProperties(property);
      for (Value value : current.getProperties(property)) {
        def.addProperty(property, value);
      }
    }

    def.removeProperties("dbVersion");
    def.setProperty("dbVersion", dbVersion);

    return def;
  }

  /**
   * Saves the specified configuration. Uses the defaultConfiguration as template. Only the
   * differences are saved!
   */
  public final void saveConfig(JsonResource config) throws DaoException {
    ConfigDao dao = get(ConfigDao.class);

    JsonResource def = dao.get("defaultConfig");
    JsonResource update = new JsonResource();

    for (String v : config.getDefinedProperties()) {
      for (Value value : config.getProperties(v)) {
        if (!def.getProperties(v).contains(value) && !("dbVersion".equals(v))) {
          update.addProperty(v, value);
        }
      }
    }

    dao.updateConfig("config", update);
  }

  /**
   * Checks if the config table exists. If it does, it is assumed that the other tables do exist as
   * well (maybe an upgrade is necessary though).
   */
  public final boolean isInstalled() {
    try {
      boolean ok = false;

      DatabaseMetaData metaData = get().getMetaData();
      ResultSet tables = metaData.getTables(null, null, "config", null);

      if (tables.next()) {
        ok = true;
      }

      tables.close();

      return ok;
    } catch (SQLException e) {
      e.printStackTrace();
      return false;
    }
  }

  /** Returns the current database version. */
  public final int getCurrentVersion() throws DaoException {
    return get(ConfigDao.class).get("defaultConfig").getProperty("dbVersion").asInteger();
  }

  /** Must return the required version for the implementing ConnectionProvider. */
  public abstract int getRequiredVersion() throws DaoException;

  @Override
  public int getUserId() {
    return userId;
  }

  /** Use this method to change the user id. Only necessary are a successful login. */
  public void updateUserId(int userId) {
    this.userId = userId;
  }
}
