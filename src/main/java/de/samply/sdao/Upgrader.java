/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.sdao;

import de.samply.sdao.json.JsonResource;
import de.samply.sdao.json.Value;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Executes an upgrade from the current database version to the required one. Requires that
 * <b>all</b> necessary UpgradeExecutors are available, named as <code>Upgrade${FROM}to${TO}</code>,
 * where FROM and TO are numbers. Those executors must be in the xx.upgrade package, where xx is the
 * connection providers package name.
 */
public class Upgrader extends SqlConnection {

  private static final Logger logger = LoggerFactory.getLogger(Upgrader.class);

  /**
   * Initializes this Upgrade with the given connection provider. The connection providers name is
   * used to create all necessary UpgradeExecutors.
   */
  public Upgrader(ConnectionProvider provider) {
    super(provider);
  }

  /** Starts the incremental upgrade. Does not commit. */
  public boolean upgrade(boolean dryRun) throws DaoException {

    try {
      /*
       * The upgrade process is separated in two steps: first all SQL files are executed, then all
       * java upgrade code is executed. This order is necessary, because the ORM in the java code
       * might expect columns, that do not exist yet.
       */
      List<UpgradeExecutor> executors = new ArrayList<>();

      logger.info("Checking for upgrade executor classes");

      for (int from = provider.getCurrentVersion(); from < provider.getRequiredVersion(); ++from) {
        int to = from + 1;
        logger.info("Checking for upgrade executor from " + from + " to " + to);

        String pkg = provider.getClass().getPackage().getName();
        Class<?> clazz = Class.forName(pkg + ".upgrade.Upgrade" + from + "to" + to);
        Constructor<?> constructor = clazz.getDeclaredConstructor(ConnectionProvider.class);
        constructor.setAccessible(true);
        Object instance = constructor.newInstance(provider);

        if (instance instanceof UpgradeExecutor) {
          UpgradeExecutor executor = (UpgradeExecutor) instance;
          executors.add(executor);

          JsonResource config = executor.getDefaultConfig();
          Value version = config.getProperty("dbVersion");

          if (version == null || version.asInteger() != to) {
            logger.error("The UpgradeExecutor does not return the right database version.");
            throw new DaoException("Upgrade class does not return the right database version.");
          }

          logger.debug(
              "Upgrade class " + executor.getClass().getCanonicalName() + " found, adding to list");
        } else {
          throw new DaoException(
              "The class " + clazz.getCanonicalName() + " does not extend the UpgradeExecutor.");
        }
      }

      logger.info("Starting SQL upgrade");
      for (UpgradeExecutor executor : executors) {
        executor.executeFiles();
        if (!executor.executeUpgrade(dryRun)) {
          logger.error("Upgrading the database failed...");
          throw new DaoException("Upgrade failed!");
        } else {
          logger.info(
              "Upgrade "
                  + executor.getClass().getCanonicalName()
                  + " from so far successful. Trying to update the defaultConfig in the config "
                  + "table.");

          JsonResource config = executor.getDefaultConfig();

          logger.debug("Upgrade successful, updating the defaultConfig.");
          provider.get(ConfigDao.class).updateConfig("defaultConfig", config);
        }
      }
    } catch (ClassNotFoundException
        | NoSuchMethodException
        | SecurityException
        | InstantiationException
        | IllegalAccessException
        | IllegalArgumentException
        | InvocationTargetException e) {
      logger.error("Error upgrading the database", e);
      throw new DaoException("Exception caught:", e);
    }

    return true;
  }
}
