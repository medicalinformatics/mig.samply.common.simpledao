/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.sdao;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Column.ColumnValue;
import de.samply.sdao.definition.Table;
import de.samply.sdao.json.BooleanLiteral;
import de.samply.sdao.json.JsonResource;
import de.samply.sdao.json.StringLiteral;
import de.samply.sdao.json.TimestampLiteral;
import de.samply.string.util.StringUtil;
import de.samply.string.util.StringUtil.Builder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** This class offers various methods to execute SQL statements easily. */
public class SqlConnection {

  private static final Logger logger = LoggerFactory.getLogger(SqlConnection.class);

  /** The connection provider. */
  protected ConnectionProvider provider;

  /** Initializes this SQL connection. */
  protected SqlConnection(ConnectionProvider provider) {
    this.provider = provider;
  }

  /** Returns a string with the selected fields ("su"."id" as "su_id", ...). */
  protected static String getSelectFields(Class<?>... classes) {
    List<Field> columns = new ArrayList<>();

    for (Class<?> c : classes) {
      for (Field f : getColumns(c)) {
        if (!columns.contains(f)) {
          columns.add(f);
        }
      }
    }

    return StringUtil.join(
        columns,
        ", ",
        new Builder<Field>() {
          @Override
          public String build(Field o) {
            try {
              Column col = (Column) o.get(null);
              if (col.distinct) {
                return "DISTINCT(\""
                    + col.table.alias
                    + "\".\""
                    + col.column
                    + "\") AS \""
                    + col.alias
                    + "\"";
              } else {
                return "\""
                    + col.table.alias
                    + "\".\""
                    + col.column
                    + "\" AS \""
                    + col.alias
                    + "\"";
              }
            } catch (IllegalArgumentException | IllegalAccessException e) {
              e.printStackTrace();
              return "ERROR!";
            }
          }
        });
  }

  /** Returns the fields for columns for the given class. */
  protected static List<Field> getColumns(Class<?> clazz) {
    List<Field> fields = new ArrayList<>();

    for (Field f : clazz.getFields()) {
      if (Modifier.isStatic(f.getModifiers()) && f.getType().equals(Column.class)) {
        fields.add(f);
      }
    }
    return fields;
  }

  /** Returns the SQL connection. Uses the provider to get one. */
  protected Connection get() throws SQLException {
    return provider.get();
  }

  /** Executes an SQL UPDATE statement and returns the count of affected rows. */
  protected int executeUpdate(String sql, Object... objects) throws DaoException {
    return executeUpdate(sql, getArrayBinder(objects));
  }

  /** Executes an SQL UPDATE statement and returns the count of affected rows. */
  private int executeUpdate(String sql, SqlBinder binder) throws DaoException {
    logger.debug("Executing " + sql);
    try (PreparedStatement statement = get().prepareStatement(sql)) {
      binder.bind(statement);
      statement.execute();
      return statement.getUpdateCount();
    } catch (SQLException e) {
      throw new DaoException("Error in UPDATE!", e);
    }
  }

  /**
   * Executes a SELECT statement, maps the result set to objects using the given ObjectMapper and
   * returns those objects in a List.
   */
  protected <A> List<A> executeSelect(String sql, ObjectMapper<A> mapper, Object... objects)
      throws DaoException {
    return executeSelect(sql, getArrayBinder(objects), mapper);
  }

  /**
   * Executes a SELECT statement, maps the result set to objects using the given ObjectMapper and
   * returns those objects in a List.
   */
  protected <A> List<A> executeSelect(String sql, SqlBinder binder, ObjectMapper<A> mapper)
      throws DaoException {
    logger.debug("Executing " + sql);
    try (PreparedStatement statement = get().prepareStatement(sql)) {
      binder.bind(statement);

      ResultSet resultSet = statement.executeQuery();
      List<A> target = new ArrayList<>();

      while (resultSet.next()) {
        A obj = mapper.getObject(resultSet);
        if (obj != null) {
          target.add(obj);
        }
      }

      resultSet.close();

      return target;
    } catch (SQLException e) {
      throw new DaoException("Error in SELECT!", e);
    }
  }

  /**
   * Executes a SELECT statement, maps the result set to objects using the given ObjectMapper.
   * Returns the <b>first</b> object only. If there are no objects in the result set, this method
   * returns null. If there is more than one object, this method throws an exception.
   */
  protected <A> A executeSingleSelect(String sql, ObjectMapper<A> mapper, Object... objects)
      throws DaoException {
    return executeSingleSelect(sql, getArrayBinder(objects), mapper);
  }

  /**
   * Executes a SELECT statement, maps the result set to objects using the given ObjectMapper.
   * Returns the <b>first</b> object only. If there are no objects in the result set, this method
   * returns null. If there is more than one object, this method throws an exception.
   */
  private <A> A executeSingleSelect(String sql, SqlBinder binder, ObjectMapper<A> mapper)
      throws DaoException {
    List<A> result = executeSelect(sql, binder, mapper);

    if (result.size() == 1) {
      return result.get(0);
    } else if (result.size() > 1) {
      throw new DaoException("More results than expected");
    } else {
      return null;
    }
  }

  /** Execute an insert statement and returns the returned primary key. */
  protected int insertRaw(String table, String fields, Object... object) throws DaoException {
    StringBuilder builder = new StringBuilder();
    builder.append("INSERT INTO ").append(table);
    builder.append(" (").append(fields);
    builder.append(") VALUES ").append(constructBinds(object));

    String sql = builder.toString();
    logger.debug("Executing " + sql);

    try (PreparedStatement statement =
        get().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
      SqlBinder binder = getArrayBinder(object);
      binder.bind(statement);

      statement.execute();

      ResultSet generatedKeys = statement.getGeneratedKeys();
      if (generatedKeys.next()) {
        return generatedKeys.getInt(1);
      } else {
        throw new DaoException("Insert Failed!");
      }
    } catch (SQLException e) {
      throw new DaoException("ERROR in Insert!", e);
    }
  }

  /** Inserts the default values into the specified table. */
  protected int insertDefault(String table) throws DaoException {
    StringBuilder builder = new StringBuilder();
    builder.append("INSERT INTO ").append(table);
    builder.append(" DEFAULT VALUES ");

    String sql = builder.toString();
    logger.debug("Executing " + sql);

    try (PreparedStatement statement =
        get().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

      statement.execute();

      ResultSet generatedKeys = statement.getGeneratedKeys();
      if (generatedKeys.next()) {
        return generatedKeys.getInt(1);
      } else {
        throw new DaoException("Insert Failed!");
      }
    } catch (SQLException e) {
      throw new DaoException("ERROR in Insert!", e);
    }
  }

  /** Returns a SqlBinder for the given array of Objects. */
  private SqlBinder getArrayBinder(final Object... object) {
    return new SqlBinder() {

      private Logger logger = LoggerFactory.getLogger("de.samply.sdao.ArrayBinder");

      @Override
      public void bind(PreparedStatement statement) throws SQLException {
        int index = 0;
        if (object != null) {
          for (int i = 0; i < object.length; ++i) {
            if (object[i] instanceof List<?>) {
              List<?> list = (List<?>) object[i];
              for (Object o : list) {
                statement.setObject(++index, o);
                logger.trace("Binding " + o.toString());
              }
            } else if (object[i] instanceof SqlEnum) {
              statement.setObject(++index, object[i].toString());
              logger.trace("Binding " + object[i].toString());
            } else if (object[i] instanceof JsonResource) {
              statement.setObject(++index, ((JsonResource) object[i]).toJson());
              logger.trace("Binding " + ((JsonResource) object[i]).toJson());
            } else {
              if (!(object[i] instanceof SqlFunction)) {
                statement.setObject(++index, object[i]);
                logger.trace("Binding " + object[i]);
              }
            }
          }
        }
      }
    };
  }

  /** Constructs a string of "values" for an array of objects. */
  private String constructBinds(Object... object) {
    return "("
        + StringUtil.join(
            object,
            ", ",
            new Builder<Object>() {
              @Override
              public String build(Object o) {
                return construct(o);
              }
            })
        + ")";
  }

  /**
   * Returns the corresponding "SQL-Value" for the specified object.
   *
   * <p>?::"SQL-ENUM" for enums CAST(? AS JSON) for JsonResource ? for everything else
   */
  private String construct(Object o) {
    if (o instanceof SqlFunction) {
      return ((SqlFunction) o).getMethod();
    }
    if (o instanceof SqlEnum) {
      SqlEnum e = (SqlEnum) o;
      return "?::\"" + e.getSqlName() + "\"";
    }
    if (o instanceof JsonResource) {
      return "CAST(? AS JSON)";
    } else {
      return "?";
    }
  }

  /** Returns a JsonResource using the specified JSON string. */
  protected JsonResource asJson(String input) {
    Gson gson = new Gson();
    JsonResource resource = new JsonResource();
    addValues(resource, gson.fromJson(input, JsonObject.class));
    return resource;
  }

  /** Converts the specified JsonObject into a JsonResource. */
  private void addValues(JsonResource resource, JsonObject obj) {
    for (Entry<String, JsonElement> entry : obj.entrySet()) {
      addValues(resource, entry.getKey(), entry.getValue());
    }
  }

  /** Add the specified JsonElement to the specified JsonResource. Works recursively. */
  private void addValues(JsonResource resource, String property, JsonElement element) {
    if (element.isJsonPrimitive()) {
      if (element.getAsJsonPrimitive().isBoolean()) {
        resource.addProperty(property, new BooleanLiteral(element.getAsBoolean()));
      } else if (element.getAsJsonPrimitive().isNumber()) {
        resource.addProperty(property, element.getAsNumber());
      } else {
        String str = element.getAsJsonPrimitive().getAsString();
        try {
          resource.addProperty(property, new TimestampLiteral(Timestamp.valueOf(str)));
        } catch (IllegalArgumentException e) {
          resource.addProperty(property, new StringLiteral(str));
        }
      }
    }

    if (element.isJsonArray()) {
      for (JsonElement e : element.getAsJsonArray()) {
        addValues(resource, property, e);
      }
    }

    if (element.isJsonObject()) {
      JsonResource res = new JsonResource();
      addValues(res, element.getAsJsonObject());
      resource.addProperty(property, res);
    }
  }

  protected int getUserId() {
    return provider.getUserId();
  }

  /** Inserts the given values into the given table. */
  protected int insert(Table table, ColumnValue... values) throws DaoException {
    return insert(table.table(), values);
  }

  /** Inserts the given values into the given table. */
  protected int insert(String table, ColumnValue... values) throws DaoException {
    List<Object> objects = new ArrayList<>();

    if (values != null) {
      for (ColumnValue cv : values) {
        objects.add(cv.value);
      }
    }

    return insertRaw(
        table,
        StringUtil.join(
            values,
            ", ",
            new Builder<ColumnValue>() {
              @Override
              public String build(ColumnValue o) {
                return o.column.column();
              }
            }),
        objects.toArray());
  }

  /** Updates the given values in the given table with the given where criteria. */
  protected int update(Table table, ColumnValue where, ColumnValue... values) throws DaoException {
    List<Object> objects = new ArrayList<>();

    if (values != null) {
      for (ColumnValue cv : values) {
        objects.add(cv.value);
      }
    }

    objects.add(where.value);

    return executeUpdate(
        "UPDATE "
            + table.table()
            + " SET "
            + StringUtil.join(
                values,
                ", ",
                new Builder<ColumnValue>() {
                  @Override
                  public String build(ColumnValue o) {
                    if (o.value instanceof JsonResource) {
                      return o.column.column() + " = CAST(? AS JSON)";
                    } else if (o.value instanceof SqlFunction) {
                      return o.column.column() + " = " + ((SqlFunction) o.value).getMethod();
                    } else if (o.value instanceof UUID) {
                      return o.column.column() + " = ?::uuid";
                    } else {
                      return o.column.column()
                          + " = ?"
                          + (o.value instanceof SqlEnum
                              ? "::\"" + ((SqlEnum) o.value).getSqlName() + "\""
                              : "");
                    }
                  }
                })
            + " WHERE "
            + where.column.column()
            + " = ?",
        objects.toArray());
  }

  protected void delete(Table table, ColumnValue id) throws DaoException {
    executeUpdate(
        "DELETE FROM " + table.table() + " WHERE " + id.column.column() + " = ?", id.value);
  }

  protected void delete(Table table, String column, Collection<Integer> ids) throws DaoException {
    executeUpdate(
        "DELETE FROM "
            + table.table()
            + " WHERE "
            + "\""
            + column
            + "\""
            + " IN ( "
            + StringUtil.join(
                ids,
                ", ",
                new Builder<Integer>() {
                  @Override
                  public String build(Integer integer) {
                    return Integer.toString(integer);
                  }
                })
            + " )");
  }

  /**
   * Reads all strings from the reader and executes them as SQL statements. Uses ';' as separator.
   * This method does <b>not</b> work for statements that define new functions in SQL.
   */
  public void executeStream(Reader reader) throws DaoException {
    String s = null;
    StringBuffer sb = new StringBuffer();

    BufferedReader br = new BufferedReader(reader);

    try {
      while ((s = br.readLine()) != null) {
        sb.append(s).append("\n");
      }
      br.close();

      // here is our splitter ! We use ";" as a delimiter for each request
      // then we are sure to have well formed statements
      executeSql(sb.toString());

    } catch (SQLException e) {
      logger.error("SQL Exception: ", e);
      logger.error("SQL Exception: ", e.getNextException());
    } catch (Exception e) {
      logger.error("*** Error : " + e.toString());
      logger.error("*** ");
      logger.error("*** Error : ", e);
      logger.error("################################################");
      throw new DaoException("Errors executing stream: ", e);
    }
  }

  /**
   * Executes all SQL statements in the given String. This method should word for any SQL
   * statements.
   */
  public void executeSql(String sql) throws SQLException {
    try (Statement st = get().createStatement()) {
      st.execute(sql);
      logger.debug("Executing " + sql);
      st.close();
    }
  }

  /**
   * Parses the given file as SQL file and executes all statements.
   *
   * @param filename file path
   */
  public void executeFile(String filename) {
    try {
      try (InputStreamReader fr =
          new InputStreamReader(new FileInputStream(new File(filename)), StandardCharsets.UTF_8)) {
        executeStream(fr);
        fr.close();
      }
    } catch (Exception e) {
      logger.error("*** Error : " + e.toString());
      logger.error("*** ");
      logger.error("*** Error : ", e);
      logger.error("################################################");
    }
  }
}
