/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.sdao.json;

import java.io.Serializable;
import java.sql.Timestamp;

/** A value used in resources. */
public abstract class Value implements Serializable {

  private static final long serialVersionUID = -6742765505665577100L;

  public abstract String getValue();

  /**
   * Trys to convert the value to an integer. Returns 0 if this operation is not possible.
   *
   * @return
   */
  public int asInteger() {
    if (this instanceof NumberLiteral) {
      return ((NumberLiteral) this).get().intValue();
    } else {
      try {
        return Integer.parseInt(getValue());
      } catch (NumberFormatException e) {
        return 0;
      }
    }
  }

  /**
   * Trys to convert the value to a float. Returns 0 if this operation is not possible.
   *
   * @return
   */
  public float asFloat() {
    if (this instanceof NumberLiteral) {
      return ((NumberLiteral) this).get().floatValue();
    } else {
      try {
        return Float.parseFloat(getValue());
      } catch (NumberFormatException e) {
        return 0;
      }
    }
  }

  /**
   * Trys to convert the value to a long. Returns 0 if this operation is not possible.
   *
   * @return
   */
  public long asLong() {
    if (this instanceof NumberLiteral) {
      return ((NumberLiteral) this).get().longValue();
    } else {
      try {
        return Long.parseLong(getValue());
      } catch (NumberFormatException e) {
        return 0;
      }
    }
  }

  /**
   * If this value is a JsonResource, this method returns this instance as JsonResource, otherwise
   * null.
   */
  public JsonResource asJsonResource() {
    if (this instanceof JsonResource) {
      return (JsonResource) this;
    } else {
      return null;
    }
  }

  /**
   * If this value is a TimestampLiteral, this method returns this instance as TimestampLiteral.
   */
  public Timestamp asTimestamp() {
    if (this instanceof TimestampLiteral) {
      return ((TimestampLiteral) this).value;
    } else {
      return null;
    }
  }

  /**
   * If this value is a BooleanLiteral, this methods returns this instance as BooleanLiteral.
   */
  public Boolean asBoolean() {
    if (this instanceof BooleanLiteral) {
      return ((BooleanLiteral) this).value;
    } else {
      return false;
    }
  }

  @Override
  public abstract boolean equals(Object o);
}
