/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.sdao.json;

import java.math.BigDecimal;

/** A storable NumberLiteral, can store ints, longs, floats and doubles. */
public class NumberLiteral extends Literal<Number> {

  private static final long serialVersionUID = -1018469461971079115L;

  public NumberLiteral(Number value) {
    super(value);
  }

  @Override
  public String getValue() {
    return "" + value;
  }

  public String toString() {
    return value + "::number";
  }

  @Override
  public boolean equals(Object o) {
    // Because Numbers aren't really comparable, we just
    // use BigInteger and compare those
    if (o instanceof NumberLiteral) {
      return new BigDecimal(this.value.toString())
              .compareTo(new BigDecimal(((NumberLiteral) o).get().toString()))
          == 0;
    } else {
      return false;
    }
  }
}
