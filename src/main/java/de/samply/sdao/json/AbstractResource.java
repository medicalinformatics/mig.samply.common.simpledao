/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.sdao.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/** Abstract resource. Either a JSON resource or a real resource. */
public abstract class AbstractResource extends Value {

  private static final long serialVersionUID = -6465529019630940439L;

  private HashMap<String, ArrayList<Value>> properties;

  protected AbstractResource() {
    this.properties = new HashMap<>();
  }

  HashMap<String, ArrayList<Value>> get() {
    return properties;
  }

  /** Resets all properties, but leaves everything else intact. */
  void reset() {
    properties.clear();
  }

  /**
   * Returns the first value for the given property. If multiple values are available, this method
   * should not be used.
   */
  public Value getProperty(String property) {
    if (properties.containsKey(property)) {
      return properties.get(property).get(0);
    } else {
      return null;
    }
  }

  /**
   * Returns all values for the given property.
   */
  public ArrayList<Value> getProperties(String property) {
    if (properties.get(property) == null) {
      return new ArrayList<Value>();
    } else {
      return properties.get(property);
    }
  }

  /**
   * Adds a Value.
   */
  public void addProperty(String property, Value value) {
    if (properties.containsKey(property)) {
      properties.get(property).add(value);
    } else {
      setProperty(property, value);
    }
  }

  /**
   * Adds a StringLiteral value.
   */
  public void addProperty(String property, String value) {
    addProperty(property, new StringLiteral(value));
  }

  /**
   * Adds a NumberLiteral value.
   */
  public void addProperty(String property, Number value) {
    addProperty(property, new NumberLiteral(value));
  }

  /**
   * Adds a BooleanLiteral value.
   */
  public void addProperty(String property, Boolean value) {
    addProperty(property, new BooleanLiteral(value));
  }

  /**
   * Sets a Value. All existing values for the given property will be deleted.
   */
  public void setProperty(String property, Value value) {
    ArrayList<Value> values = new ArrayList<>();
    values.add(value);
    properties.put(property, values);
  }

  /**
   * Sets a StringLiteral value. All existing values for the given property will be deleted.
   */
  public void setProperty(String property, String value) {
    setProperty(property, new StringLiteral(value));
  }

  /**
   * Sets a NumberLiteral value. All existing values for the given property will be deleted.
   */
  public void setProperty(String property, Number value) {
    setProperty(property, new NumberLiteral(value));
  }

  /**
   * Sets a BooleanLiteral value. All existing values for the given property will be deleted.
   */
  public void setProperty(String property, Boolean value) {
    setProperty(property, new BooleanLiteral(value));
  }

  /**
   * Returns all defined properties for this resource.
   */
  public Set<String> getDefinedProperties() {
    return properties.keySet();
  }

  /**
   * Removes all property values from the given property.
   */
  public void removeProperties(String property) {
    properties.remove(property);
  }

  /**
   * Removes a single property value from the given property.
   */
  public void removeProperty(String property, Value value) {
    if (properties.containsKey(property)) {
      for (Value v : properties.get(property)) {
        if (v.equals(value)) {
          properties.get(property).remove(v);
        }
      }
    }
  }

  public String toJson() {
    return ResourceUtils.createJsonElement(this).toString();
  }
}
