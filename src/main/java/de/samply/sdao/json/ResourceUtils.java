/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.sdao.json;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class ResourceUtils {

  /**
   * TODO: add javadoc.
   */
  public static DiffResource createDiffResource(AbstractResource from, AbstractResource to) {
    JsonResource plus = new JsonResource();
    JsonResource minus = new JsonResource();

    for (String p : from.getDefinedProperties()) {
      for (Value f : from.getProperties(p)) {
        if (!(f instanceof Literal<?>)) {
          continue;
        }

        boolean found = false;
        ArrayList<Value> tvalues = to.getProperties(p);
        if (tvalues != null) {

          for (Value t : tvalues) {
            if (f.equals(t)) {
              found = true;
            }
          }
        }
        if (!found) {
          minus.addProperty(p, f);
        }
      }
    }

    for (String p : to.getDefinedProperties()) {
      for (Value f : to.getProperties(p)) {
        if (!(f instanceof Literal<?>)) {
          continue;
        }

        boolean found = false;
        ArrayList<Value> tvalues = from.getProperties(p);
        if (tvalues != null) {

          for (Value t : tvalues) {
            if (f.equals(t)) {
              found = true;
            }
          }
        }
        if (!found) {
          plus.addProperty(p, f);
        }
      }
    }

    return new DiffResource(plus, minus);
  }

  /**
   * Erzeugt aus der Property-Values-Map ein JSON-Objekt.
   */
  public static JsonObject createJsonObject(HashMap<String, ArrayList<Value>> values) {
    JsonObject data = new JsonObject();
    for (Entry<String, ArrayList<Value>> entry : values.entrySet()) {
      if (entry.getValue().size() == 1) {
        Value value = entry.getValue().get(0);
        data.add(entry.getKey(), createJsonElement(value));
      } else {
        JsonArray array = new JsonArray();
        for (Value value : entry.getValue()) {
          array.add(createJsonElement(value));
        }
        data.add(entry.getKey(), array);
      }
    }
    return data;
  }

  /**
   * Erzeugt aus dem Value value ein passendes JSON-Value.
   */
  static JsonElement createJsonElement(Value value) {
    if (value instanceof NumberLiteral) {
      return new JsonPrimitive(((NumberLiteral) value).get());
    } else if (value instanceof TimestampLiteral) {
      return new JsonPrimitive(((TimestampLiteral) value).get().toString());
    } else if (value instanceof JsonResource) {
      HashMap<String, ArrayList<Value>> values = ((AbstractResource) value).get();
      return createJsonObject(values);
    } else if (value instanceof BooleanLiteral) {
      return new JsonPrimitive(value.asBoolean());
    } else {
      return new JsonPrimitive(value.getValue());
    }
  }

  public static class DiffResource {

    public final JsonResource plus;
    public final JsonResource minus;

    DiffResource(JsonResource plus, JsonResource minus) {
      this.plus = plus;
      this.minus = minus;
    }
  }
}
