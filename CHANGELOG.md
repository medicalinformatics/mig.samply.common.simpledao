# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.0.0] - 2019-11-29
### Changed
- apply google java code style, renamed some methods/classes
### Fixed
- delete method correctly working with camel case column names

## [2.0.0] - 2018-12-17
### Changed
- update samply.common.config to 3.0.0

## [1.9.3] - 2018-08-13
### Changed
- instead of executing all sql files first and then the java code, execute the upgrade iterations
 in order

## [1.9.2] - 2018-07-20
### Added
- added method to delete multiple entries via a collection of ids

## [1.9.1] - 2017-06-01
### Added
- added some more debugger output when an exception is thrown

## [1.9.0]
### Changed
- the upgrader now uses a two step upgrade, first it executes all sql files, then all java code
- switched to SLF4J
### Fixed
- fixed the logger output of the binder to `TRACE`

## [1.8.0] - 2015-12-21
### Changed
- Open Source
- updated dependencies

## [1.7.0]
### Added
- added the first maven site documentation
- added support for UUIDs
- added missing license headers
